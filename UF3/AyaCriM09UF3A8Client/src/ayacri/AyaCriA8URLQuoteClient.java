package ayacri;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class AyaCriA8URLQuoteClient {

	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
            System.err.println("Usage:  java AyaCriA8URLQuoteClient http://<location of your servlet/script>");
            System.exit(1);
        }
		Scanner input = new Scanner(System.in);
		System.out.println("Which quote do you want? Write a number.");
    	int quote = input.nextInt();
		
		
		URL url = new URL(args[0]);
		URLConnection connection = url.openConnection();
		connection.setDoOutput(true);
		
		PrintWriter out = new PrintWriter(connection.getOutputStream(), true);
		out.write("quote=" + quote);
		out.close();
		
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String readLine;
		while ((readLine = in.readLine()) != null) {
            System.out.println(readLine);
        }
        in.close();
        input.close();
	}

}
