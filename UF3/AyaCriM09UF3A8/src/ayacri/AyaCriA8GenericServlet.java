package ayacri;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.GenericServlet;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

/**
 * Servlet implementation class AyaCriA8GenericServlet
 */
@WebServlet("/AyaCriA8GenericServlet")
public class AyaCriA8GenericServlet extends GenericServlet implements Servlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see GenericServlet#GenericServlet()
     */
    public AyaCriA8GenericServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#service(ServletRequest request, ServletResponse response)
	 */
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		// This code gets a list of parameters from the client request, prints them at the
		// server side, and sends back to the client the received information
		String parameter = request.getParameter("quote");
		
		String line = Files.readAllLines(Paths.get(getServletContext().getRealPath("one-liners.txt"))).get(Integer.parseInt(parameter) - 1);
		
		response.getWriter().append(line);
	}

}
